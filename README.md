# TP – Dockerfile


**Nombre y Apellido**

Nicolas Galván

**Materia**

Infraestructura en la Nube – Jueves 

(73041-2024-1C-Infraestructuras 22-División A-Día jueves)



# Introducción
En este documento se encuentra el paso a paso de la creación de una imagen Docker.
# Requisitos
Los requisitos para implementar esta imagen son:

Sistema operativo Ubuntu

Acceso a internet
# Desarrollo
## Preparación de entorno
Previamente a iniciar con el paso a paso sobre la implementación de Docker, se debe tener un sistema operativo funcionando y con las configuraciones básicas.

Este debe contener instalado el sistema operativo Ubuntu, con un usuario configurado. Debe tener acceso a internet.

Luego de verificar esto parámetros, se debe actualizar el sistema operativo con el comando:
***sudo apt update && sudo apt upgrade***.

## Configuración de Red
Para un correcto funcionamiento y administración del entorno, se puede configurar los parámetros de red del sistema. En el repositorio se encuentra en ejemplo de las configuraciones “**Configuracion\_de\_red.txt**”, que se deben realizar en el archivo de la ubicación “**/etc/resolv.conf**”

## Instalación de Docker
Luego de la correcta actualización del sistema, instalar Docker con el comando:
***sudo apt update && sudo apt install docker docker.io -y***. 

Agregar al usuario que se está utilizando al grupo Docker con el comando: 
***sudo gpasswd -a USER Docker*** (en este caso se usó como ejemplo al usuario USER).

Para probar el correcto funcionamiento de Docker, se puede utilizar el comando:
***docker run hello-world***.

Atención: Docker necesita privilegios root para poder ejecutar contenedores, para esto puede utilizarse el comando ***sudo gpasswd -a USER sudo*** y agregar el usuario al grupo sudo, o ***sudo gpasswd -a root Docker*** para agregar al usuario root al grupo Docker y así poder ejecutar contenedores en el usuario root.


## Ejecución de servicio apache 2 en Docker
Para utilizar el servicio Apache 2 dentro de Docker se puede utilizar el comando:
***docker run -d --name="apache2-docker" -p 8283:80 ubuntu/apache2***

![Interfaz de usuario gráfica, Texto, Aplicación

Descripción generada automáticamente](Aspose.Words.0e43ae03-868a-4be1-a93f-d7a7eb90ff2a.001.png)

En este caso, se inicia el servicio de apache2 dentro de Docker, con el nombre del contenedor como “*apache2-docker*” y saliendo por el puerto “*8283*”.


Se puede visualizar la ejecución en el navegador web en:

***http://IP\_SERVIDOR\_DOCKER:8283***

## Ejecución de servicio apache 2 en Docker con volumen tipo “bind”
Para utilizar el servicio Apache 2 dentro de Docker con volumen tipo “bind” se puede utilizar el comando:

***docker run -d --name="apache2-bind " -v /srv/apache2-bind:/var/www/html -p 8181:80 ubuntu/apache2***

Luego de ejecutar el comando, se debe completar el archivo index.html que se encuentra en la ubicación “**/srv/apache2-bind**” y colocar uno de los dos ejemplos que se encuentran en el repositorio, “**Ejemplo\_avanzado.html**” o “**Ejemplo\_básico.html**” y comprobar el correcto funcionamiento del contenedor.

## Instalación y configuración proxy reverso
Para la utilización del servicio “Proxy reverso”, es necesario preferentemente otro entorno (pueden utilizarse los pasos anteriores hasta el de “Instalación de Docker”).

Una vez se tenga el entorno, se instala el servicio “nginx” con el comando:

***sudo apt install nginx***

Luego, dentro del archivo con ubicación “**/etc/nginx/sites-enabled/default**” se coloca la configuración que se encuentra en el repositorio con el nombre “**Configuracion\_proxy\_reverso.txt**”. Los datos deben modificarse según sea conveniente, como la IP y puerto.

Para efectuar los cambios, es necesario reiniciar el servicio con el comando:

***systemctl reload nginx***




# Fuentes 

Las fuentes utilizadas fueron:

- Información de las clases
- w3schools.com

